# Oranchelo-remix
This icon themes remixed from oranchelo, credit to [zayronxio](http://zayronxio.deviantart.com) and [oranchelo team](https://github.com/OrancheloTeam)

You can install by move the icons directory to `/usr/share/icons/` or `~/.local/share/icons/`

![](https://1.bp.blogspot.com/-GE1uQHWluAg/WV5eXB3tU5I/AAAAAAAAAgA/jVpK1X-k_gogAT4XH8R7Jix2xePpAPoQgCLcBGAs/s1600/Screenshot%2Bfrom%2B2017-07-06%2B22-58-16.png)
